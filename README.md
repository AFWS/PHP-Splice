# APPLICATION TITLE: PHP Splice

 **AUTHOR:**     Jim S. Smith

 **COPYRIGHT:**  (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**    GPL 2.0 or > (*with proper attribution please*)

 **VERSION:**    2.01.06, *Updated:* 2023/12/24

 **USE:**        To split or merge split files.

 **DESCRIPTION:**

    A PHP-CLI script to split larger files into smaller "chunks", or
    merge (that is "combine/concatenate") smaller "chunked" files into
    a larger one.

    *Therefore, this script operates in one of two modes (or, "actions"):*

    'split' - Which means to take a larger file, and split it into smaller
    "chunks" to the size, in bytes ( as determined by the option "chunk" ).
    This mode will produce several smaller files marked with a sequence number
    affixed to the end of the filename.

    'merge' - Receives one or several files and combines them into one. Each
    "split-file" must be related, that is - originating from the same original
    file. 'Merge' is the default "action" argument, if none is goven.

 **REQUIREMENTS:**

 1. Should work in any PHP-CLI version,
 2. File-system functions, like 'fopen()' and 'fwrite()' must be usable,

 **OTHER NOTES:**

 1. GREAT CARE should be taken when using this utility!

 2. Multiple file-paths used in 'source' for merging files must all be enclosed in "
    ( "double-quotes" ).

 3. Of course, larger files will take longer to "split" than smaller ones.

 4. This script does not handle any wildcard characters in the file-paths.

 5. Any file-path argument which contains any spaces, or certain special meta-
    characters, will need to be enclosed in 's ( *single-quotes* ).

 6. This script can be copied to /usr/bin to make it available, server-wide.
    ( *Just make sure its permissions are set to: 0755.* )

 **WARNINGS:**

    Be very careful not to mix-and-match files for combining!

    The source-files to be used in a merge operation must all be from the original
    singular file in whch they were generated from.

    All merge operations will start by looking for a filename with the sequence ".001"
    appended to the filename. If this first file is missing, the script will abort
    with a "file not found" error.

**LEGAL NOTICE:**

    Redistribution and use in source and binary forms, with or without modification,
    *are permitted provided that the following conditions are met:*

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    RECOMMENDED PHP VERSION: >= 7.3 (highly-suggested, anyway)
